<?php


namespace Aghandeh\IranShippingPrice;
use Illuminate\Support\ServiceProvider;

class IranShippingPriceServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'/Database/migrations');
        $this->publishes([
            __DIR__.'/config/shipping.php' => config_path('shipping.php'),
        ]);
    }
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/config/shipping.php', 'shipping'
        );
    }
}
