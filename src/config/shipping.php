<?php

return [
    'sefareshi' => [
        'extra_cost_percent' => 0,
        'extra_cost' => 0,
        'insurance' => 650,
        'tax' => 9,
        500 => [
            'in' => 3800,
            'beside' => 4900,
            'out' => 5300
        ],
        1000 => [
            'in' => 5000,
            'beside' => 6800,
            'out' => 7300
        ],
        2000 => [
            'in' => 6900,
            'beside' => 8800,
            'out' => 9500
        ],
        9999 => [
            'in' => 2500,
            'beside' => 3000,
            'out' => 3500
        ],
    ],
    'pishtaz' => [
        'extra_cost_percent' => 0,
        'extra_cost' => 0,
        'insurance' => 650,
        'tax' => 9,
        500 => [
            'in' => 5750,
            'beside' => 7800,
            'out' => 8400
        ],
        1000 => [
            'in' => 7400,
            'beside' => 10000,
            'out' => 11200
        ],
        2000 => [
            'in' => 9800,
            'beside' => 12700,
            'out' => 14000
        ],
        9999 => [
            'in' => 2500,
            'beside' => 3000,
            'out' => 3500
        ],
    ],
];
